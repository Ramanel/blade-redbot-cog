# Retrieve the modules
import discord
from discord.utils import *
import random
import re
import asyncio
from PIL import Image, ImageDraw, ImageFont, ImageFilter
from io import BytesIO
import functools

#Declare Match class
class Match:

    #----------Setup the match-----------

    # Add specific properties to match instance
    def __init__(self, bot, player1, player2, versionNumber, bladeConfig):
        self.bot = bot
        self.cogPath = bladeConfig['cogPath']
        self.messageExpireTime = bladeConfig['messageExpireTime']
        self.embedConfig = bladeConfig['embed']
        self.versionsConfig = bladeConfig['versions']
        self.phases = bladeConfig['phases']
        self.cards = bladeConfig['cards']
        self.stage = {
            'hands': [[],[]],
            'fields': [[],[]],
            'decks': [[],[]],
            'tempCards': []
        }
        self.players = [
            {'player': player1, 'msg': None, 'reactLock': False, 'speech': None, 'awaitSpeech': False},
            {'player': player2, 'msg': None, 'reactLock': False, 'speech': None, 'awaitSpeech': False}
        ]
        self.currentPlayerIndex = 0
        self.turnPhase = 0
        self.upcomingPhase = 1
        self.gameVersion = 'blade1' if versionNumber == '1' else 'blade2'
        self.assets = None

    # Setup function to start the match
    async def start(self):

        # Send the players a loading screen (embed)
        # Also save both sent messages into the match object.
        for i in range(len(self.players)):

            #Send message
            msg = await self.players[i]['player'].send(embed =
                discord.Embed(
                    description = 'Loading the game...',
                    color = int(self.embedConfig['color'], 0)
                )
            )

            # Save the msg into the match object
            self.players[i]['msg'] = msg

        #Deal the cards
        self.deal()

        #Start phase 1 to determine the starting player
        await self.nextPhase()



    #----------Add function to control the user input-----------

    #Wait for valid input of one or both players, advance to the next match phase and wait an optional speech input again
    #This function is recursively called by the nextPhase function
    async def awaitInput(self):

        #Wait for a message
        def verifyInput(m):
            cPlayer = self.getCurrentPlayer()
            cPhase = self.getCurrentPhase()

            #WHen the player that reacted is not one of the players, is a bot or there is no current player, return false
            rPlayer = find(lambda p: p['player'].id == m.author.id, self.players)
            if rPlayer is not None and cPlayer is not None and not m.author.bot:
                rpIndex = self.players.index(rPlayer)
            else:
                return False

            #Depending on the phase, different decks of cards are checked
            maxCardNum = [
                10,
                len(self.stage['hands'][rpIndex]),
                10 if len(self.stage['decks'][rpIndex]) > 0 else len(self.stage['hands'][rpIndex]),
                10 if len(self.stage['decks'][rpIndex]) > 0 else len(self.stage['hands'][rpIndex]),
                len(self.stage['hands'][0 if rpIndex == 1 else 0]),
                0,
                0
            ]
            return ((m.author.id == cPlayer['player'].id or cPhase['bothCanReact'])
                    and (m.channel.id == m.author.dm_channel.id)
                    and (rPlayer['reactLock'] == False)
                    and any(e[1] == m.content and maxCardNum[self.turnPhase-1] >= int(e[1]) for e in self.getCurrentPhase()['inputs']))

        message = await self.bot.wait_for('message', check=verifyInput, timeout=self.embedConfig['expireTime'])

        #Retrieve the current and opposing player indices
        pIndex = self.currentPlayerIndex
        oppIndex = 0 if self.currentPlayerIndex == 1 else 0


        #Setup the phases whose code blocks only get triggered after an reaction occurs during a certain phase

        #Phase 2: The current player tries to play a card to heighten their total score
        if self.turnPhase ==  2:

            #Get the index of the chosen card
            cardIndex = self.getInputKey(message.content) - 1

            #Only if a card exists on this index, it can be played
            #When the resulting amount of points is higher than their opponent's, the turn is passed on
            #When it's the same, new cards are drawn or otherwise chosen to determine the turn
            #When it's lower, the current player has lost
            if 0 <= cardIndex < len(self.stage['hands'][self.currentPlayerIndex]):
                await self.playCard(cardIndex)

        #Phase 1/3/4: Dicking - Both players attempt to draw a card or otherwise choose a card from their hand to determine the current player
        elif self.turnPhase in [1,3,4]:

            #Retrieve the index of the player who reacted and override the current local player index (since both are now current players)
            pIndex = None
            for i, p in enumerate(self.players):
                if p['player'].id == message.author.id:
                    pIndex = i

            #Add a reaction lock to the player who reacted
            self.players[pIndex]['reactLock'] = True

            #If this is the first player who reacted, clear the field and wait for the other player's input
            if len(list(filter(lambda player: player['reactLock'] == True, self.players))) == 1:
                self.stage['fields'] = [[], []]
                asyncio.ensure_future(self.awaitInput())

            #When the deck still has cards, draw a card
            if len(self.stage['decks'][pIndex]) > 0:
                self.drawCard(pIndex)

            #Otherwise get the index of the input the user reacted with and place that card on the field,
            else:
                cardOptionIndex = self.getInputKey(message.content) - 1
                self.placeCard(pIndex, cardOptionIndex)

            #Change the message sent to the user to the 'dOtherP' value after reaction
            #by resending the same embed with a changed description
            asyncio.ensure_future(self.changeDescription(pIndex, self.getCurrentPhase()['dOtherP']))

            #When when cards are present in both fields,
            #compare the two cards and give the next turn to the player with the lowest points.
            #When both cards have the same amount of points, advance to phase 3 for a different player message.
            if self.getTotalPoints(0) > 0 and self.getTotalPoints(1) > 0:
                asyncio.ensure_future(self.compareCards())

        #Phase 5: The current player may choose a card from their opponent's hand to discard
        elif self.turnPhase == 5:

            #Retrieve the index of the card to be discarded
            cardDiscardIndex = self.getInputKey(message.content) - 1

            #Remove the card
            self.removeCard(self.stage['hands'][oppIndex], cardDiscardIndex)

            #Reorder the cards
            self.sortCards(self.stage['hands'][oppIndex])

            #Advance again to phase 2 with the current user
            self.upcomingPhase = 2
            asyncio.ensure_future(self.nextPhase())

        #Phase 6: The player with the highest score has won
        #Phase 7: The match has ended in a draw

    # Function to listen for a message to display player's quoted messages inside the text bubble.
    # Also, a confirmation is given upon a successful message.
    async def awaitSpeech(self, pIndex):

            #Retrieve the player
            player = self.players[pIndex]

            #Only await speech when the awaitSpeech property for this player is set to False
            if player['awaitSpeech'] == False:

                #Set the await speech property to true
                player['awaitSpeech'] = True

                #Await message
                def checkSpeech(m):
                    return (re.search(r"^[\'\"][^\'\"]+[\'\"]$", m.content) is not None
                            and m.author.id == player['player'].id
                            and m.channel.id == player['msg'].channel.id)
                try:
                    speech = await self.bot.wait_for('message', check=checkSpeech, timeout=self.messageExpireTime)
                except asyncio.TimeoutError:
                    speech = None

                #Set await property to false and only set the speech property and give confirmation when a message has been received
                player['awaitSpeech'] = False
                if speech is not None:
                    player['speech'] = speech.content.replace('\'', '').replace('\"', '')
                    await player['player'].send('Message received.')


    #-----------Add functions to control the match----------------

    #Add function to deal the cards to the stage.
    #A game version ((blade) 1 or 2) can be specified, which determines the amount and exclusion of certain cards.
    def deal(self):

        #Generate a deck to deal cards from
        deck = []
        for card in self.cards:

            #Get the number of times a cards should be generated
            number = card['numbers'][self.gameVersion]

            #Add the cards to the deck
            for i in range(number):
                #Create a copy of the card
                cardCopy = card.copy()

                #Then insert it
                deck.append(cardCopy)

        #Shuffle the deck in place
        deck = random.sample(deck, len(deck))

        #Deal cards to the hands and player decks
        for name, number in self.versionsConfig[self.gameVersion]['cardNumbers'].items():
            self.stage[name + 's'][0].extend(deck[:number])
            self.stage[name + 's'][1].extend(deck[number:2*number])
            deck = deck[2*number:]

        #Order both hands in ascending order
        for hand in self.stage['hands']:
            self.sortCards(hand)


    #Define function to determine whether a card is not special
    @staticmethod
    def isNotSpecial(card):
        return card['suit'] != 'Special' or card['name'] == 'Rod'

    #Add function to go to specified next turn phase
    async def nextPhase(self):

        #Relieve the reaction locks for the players
        for p in self.players:
            p['reactLock'] = False

        #When the current player has an empty hand/isn't left with any special cards (except for the rod), end the match
        if len(list(filter(self.isNotSpecial, self.stage['hands'][self.currentPlayerIndex]))) == 0:
            return self.end()

        #Go to the specified phase
        self.turnPhase = self.upcomingPhase

        #Render the embed belonging to the current phase
        await self.render()

        #Wait for input
        asyncio.ensure_future(self.awaitInput())
        asyncio.ensure_future(self.awaitSpeech(0))
        asyncio.ensure_future(self.awaitSpeech(1))

    # Add function to order cards in ascending order (with the special cards being ordered alphabetically after the rest)
    def sortCards(self, cards):
        def cardPosition(card):
            if not Match.isNotSpecial(card):
                return ord(card['name'][0])
            else:
                return card['points']
        cards.sort(key = cardPosition)

    #Add function to end the match
    def end(self):

        #Get the winner's index
        winnerIndex = self.getWinnerIndex()

        #When there is a winner, advance to phase 6 and set that person as the current user
        if winnerIndex is not None:
            self.currentPlayerIndex = winnerIndex
            self.turnPhase = 6
        #Else advance to phase 7 (=a draw)
        else:
            self.turnPhase = 7

        #Render the victory/lost/draw message
        asyncio.ensure_future(self.render())

    # Add function to get the player index of the player with the highest total points.
    # Return None, when they are equal
    def getWinnerIndex(self):

        #Retrieve the total amount of points of both players
        p1Points = self.getTotalPoints(0)
        p2Points = self.getTotalPoints(1)

        #Return the player index, whose points are higher
        if p1Points > p2Points:
            return 0
        elif p2Points > p1Points:
            return 1
        else:
            return None

    #Add function to get the total points of a player's field cards
    def getTotalPoints(self, pIndex):

        #Calculate the total score by looping over all cards in the field
        totalScore = 0
        for card in self.stage['fields'][pIndex]:

            #Only non flipped cards contribute to the points
            if not card.get('flipped', False):

                #A force card without value doubles the points
                if card['name'] == 'Force' and card['points'] == 0:
                    totalScore *= 2
                #Otherwise the points are added to the total
                else:
                    totalScore += card['points']

        return totalScore

    #Add function to retrieve the current active player
    def getCurrentPlayer(self):
        return self.players[self.currentPlayerIndex] or None

    #Add function to retrieve information about the current phase
    def getCurrentPhase(self):
        return self.phases[self.turnPhase - 1] or None

    #Add function to get the key of the emoji the player reacted with
    def getInputKey(self, inp):
        return int(find(lambda e: e[1] == inp, self.getCurrentPhase()['inputs'])[0])

    #Add function to go to the next turn
    def nextTurn(self, playerIndex = None):
        self.currentPlayerIndex = playerIndex if playerIndex is not None else (1 if self.currentPlayerIndex == 0 else 0)
        self.upcomingPhase = 2
        asyncio.ensure_future(self.nextPhase())

    #Add function to play a card
    async def playCard(self, handCardIndex):

        #Retrieve the card
        card = self.stage['hands'][self.currentPlayerIndex][handCardIndex]

        #If it's a special card, something happens.
        if card['suit'] == 'Special':

            #Save the name of the special card into the speech bubble property of the current player
            #(this will be displayed for the opposing player)
            self.players[self.currentPlayerIndex]['speech'] = card['name'] + '!'

            #Carry out the ability of the special card
            fields = self.stage['fields']
            oppIndex = 1 if self.currentPlayerIndex == 0 else 0

            #Flip the opponent's last played card on the field
            if card['name'] == 'Bolt':
                fields[oppIndex][-1]['flipped'] = True
                self.removeCard(self.stage['hands'][self.currentPlayerIndex], handCardIndex)

            #Switch the two player's fields
            if card['name'] == 'Mirror':
                fields[0], fields[1] = fields[1], fields[0]
                self.removeCard(self.stage['hands'][self.currentPlayerIndex], handCardIndex)

            #When there the last card is flipped, flip it back.
            #If there is no last card, just place the card
            if card['name'] == 'Rod':
                lastCard = fields[self.currentPlayerIndex][-1]
                if lastCard.get('flipped', False):
                    lastCard['flipped'] = False
                    self.removeCard(self.stage['hands'][self.currentPlayerIndex], handCardIndex)
                else:
                    self.players[self.currentPlayerIndex]['speech'] = None
                    self.placeCard(self.currentPlayerIndex, handCardIndex)

            #Just place the card, but change change the value to '0' before placing it (the getTotalPoints function then doubles the points)
            if card['name'] == 'Force':
                self.stage['hands'][self.currentPlayerIndex][handCardIndex]['points'] = 0
                self.placeCard(self.currentPlayerIndex, handCardIndex)

            #Randomly shuffle the opponent's cards and then directly advance to phase 5
            if card['name'] == 'Blast':
                self.removeCard(self.stage['hands'][self.currentPlayerIndex], handCardIndex)
                self.stage['hands'][oppIndex] = random.sample(self.stage['hands'][oppIndex], len(self.stage['hands'][oppIndex]))
                self.upcomingPhase = 5
                return asyncio.ensure_future(self.nextPhase())

        #Otherwise the card is just placed on the field and heightens the current player's total card points.
        else:
            self.placeCard(self.currentPlayerIndex, handCardIndex)

        #Remove any flipped cards after playing a card (except for the blast card)
        cardsToRemove = []
        for i in range(len(self.stage['fields'][self.currentPlayerIndex])):
            card = self.stage['fields'][self.currentPlayerIndex][i]
            if card.get('flipped', False):
                cardsToRemove.append(i)
        for cardIndex in cardsToRemove:
            self.removeCard(self.stage['fields'][self.currentPlayerIndex], cardIndex)

        #When the current player has the most card points, the turn is passed to the other player
        if self.getWinnerIndex() == self.currentPlayerIndex:
            self.nextTurn()

        #When both players have the same total amount of points, advance to phase 3 to draw/choose a card to determine the next player
        elif self.getWinnerIndex() != 0 and self.getWinnerIndex() != 1:
            self.upcomingPhase = 3
            await self.nextPhase()

        #When the current player has a lower score, they have lost
        else:
            self.end()


    #Add function to draw a card and place it onto the field
    def drawCard(self, pIndex):

        #When a card is available draw it and place it on the field
        if len(self.stage['decks'][pIndex]) > 0:
            card = self.stage['decks'][pIndex].pop()
            self.stage['fields'][pIndex].append(card)

    #Add function to place a card on the field
    def placeCard(self, pIndex, cardIndex):

        #When a card is available on that hand position place it on the field
        if 0 <= cardIndex < len(self.stage['hands'][pIndex]):
            card = self.stage['hands'][pIndex].pop(cardIndex)
            self.stage['fields'][pIndex].append(card)

    #Add function to remove card from the a stack of cards (either a field or hand)
    def removeCard(self, cards, cardIndex):
        cards.pop(cardIndex)

    #Add function to compare the points of the cards currently on the field to determine the next player
    async def compareCards(self):

        #When the card points of both players are the same, advance to phase 4 (which has a different message for the players) to draw new cards
        if self.getWinnerIndex() is None:
            self.upcomingPhase = 4
            await self.nextPhase()

        #Otherwise the player with the lowest points gets to play their next card
        else:
            self.nextTurn(1 if self.getWinnerIndex() == 0 else 0)



    #----------Define function to render the match-----------
    async def render(self):

        #Import assets when not already available
        if self.assets is None:
            self.assets = {}
            self.assets['bgImg'] = Image.open(f'{self.cogPath}images/background2.jpg')
            self.assets['playerBorder'] = Image.open(f'{self.cogPath}images/playerBorder.png').convert("RGBA")
            self.assets['nameBorder'] = Image.open(f'{self.cogPath}images/nameBorder.png').convert("RGBA")
            self.assets['pointsBorder'] = Image.open(f'{self.cogPath}images/pointsBorder.png').convert("RGBA")
            self.assets['textBorder'] = Image.open(f'{self.cogPath}images/textBorder.png').convert("RGBA")
            self.assets['winMsg'] = Image.open(f'{self.cogPath}images/win.png').convert("RGBA")
            self.assets['loseMsg'] = Image.open(f'{self.cogPath}images/lose.png').convert("RGBA")
            self.assets['drawMsg'] = Image.open(f'{self.cogPath}images/draw.png').convert("RGBA")
            self.assets['bubbleRight'] = Image.open(f'{self.cogPath}images/bubbleRight.png').convert("RGBA")
            self.assets['bubbleMiddle'] = Image.open(f'{self.cogPath}images/bubbleMiddle.png').convert("RGBA")
            self.assets['bubbleLeft'] = Image.open(f'{self.cogPath}images/bubbleLeft.png').convert("RGBA")
            self.assets['playersImg'] = []
            self.assets['playersImg'].append(Image.open(BytesIO(await self.players[0]['player'].avatar_url.read())).convert("RGBA"))
            self.assets['playersImg'].append(Image.open(BytesIO(await self.players[1]['player'].avatar_url.read())).convert("RGBA"))
            self.assets['cardsImg'] = {}
            self.assets['numImg'] = []

            #Load the number images into the assets
            for i in range(10):
                self.assets['numImg'].append(Image.open(f'{self.cogPath}images/numbers/{i}.png'))

            #Load the card images into the assets, which are mapped by their path
            for card in self.cards:
                self.assets['cardsImg'][card['image']] = Image.open(self.cogPath + card['image'])

        #Assign assets to local variable
        assets = self.assets

        #Define the sizes to be drawn on the canvasses
        canvasWidth = 1000
        canvasHeight = 720
        colorWhite = 0xffffff
        borderOffset = 15
        cardOffset = 5
        cardWidth = 115
        cardHeight = 154
        currhandCardOverlap = -2/4 * cardWidth
        opphandCardOverlap = -55/100 * cardWidth
        fieldCardOverlap = -2/3 * cardWidth
        handHeight = 540
        handWidth = 600
        handHeightCorrection = 15
        handWidthCorrection = 55
        cardsHeight = 25
        cardNumSize = 0.75
        fieldHeight = 353
        fieldWidth = 585
        fieldHeightCorrection = 19
        fieldWidthCorrection = 5
        pointsRelHeight = 43
        pointsHeightCorrection = 17
        pointsWidthCorrection = -3
        playerBorderSize = 7/12
        pfHeight = 287
        pfWidth = 287
        currDeckWidth = 100
        oppDeckWidth = 230

        nameFont = ImageFont.truetype(f'{self.cogPath}images/Tahoma.ttf', 20)
        speechFont = ImageFont.truetype(f'{self.cogPath}images/Tahoma.ttf', 30)

        #Setup the function to send a new match screen
        async def send(pIndex):

            #Retrieve the current player
            p = self.players[pIndex]

            #Retrieve his opponent's index
            oppIndex = 0 if pIndex == 1 else 1

            #Retrieve the correct description
            #When 'bothCanReact' is set to True, the msg of 'dCurrentP' is presented to both players
            #'dOtherP' is not used, but can be used in other places if desired so.
            description = self.getCurrentPhase()['dCurrentP'] if (self.getCurrentPlayer() == p or self.getCurrentPhase()['bothCanReact']) else self.getCurrentPhase()['dOtherP']

            #Create the message
            screen = discord.Embed(
                    description = description,
                    color = int(self.embedConfig['color'],0)
                )
            screen.add_field(name='Player 1 hand', value='\n'.join([f"{c['name']} ({c['points']})" for c in self.stage['hands'][0]]) or 'Empty', inline=True)
            screen.add_field(name='Player 1 field', value='\n'.join([f"{c['name']} ({c['points']})" for c in self.stage['fields'][0]]) or 'Empty', inline=True)
            screen.add_field(name='Player 1 deck', value='\n'.join([f"{c['name']} ({c['points']})" for c in self.stage['decks'][0]]) or 'Empty', inline=True)
            screen.add_field(name='Player 2 hand', value='\n'.join([f"{c['name']} ({c['points']})" for c in self.stage['hands'][1]]) or 'Empty', inline=True)
            screen.add_field(name='Player 2 field', value='\n'.join([f"{c['name']} ({c['points']})" for c in self.stage['fields'][1]]) or 'Empty', inline=True)
            screen.add_field(name='Player 2 deck', value='\n'.join([f"{c['name']} ({c['points']})" for c in self.stage['decks'][1]]) or 'Empty', inline=True)

            #Create the canvas with background and the drawing tool
            canvas = assets['bgImg'].resize((canvasWidth, canvasHeight))
            ctx  = ImageDraw.Draw(canvas)

            #Create a function to draw an image in any size and rotation
            def drawImage(img, x, y, width = None, height = None, rotation = 0, smooth = False):

                #Make a copy of the given image
                transImg = img.copy()

                #Resize the image if dimensions are specified
                if width or height is not None:
                    if width is None:
                        width = img.width
                    if height is None:
                        height = img.height
                    transImg = transImg.resize((int(width), int(height)))
                else:
                    if width is None:
                        width = img.width
                    if height is None:
                        height = img.height

                #Rotate the image if a rotation is given
                transImg = transImg.rotate(int(rotation), expand=True, resample=Image.BICUBIC)

                #When 'smooth' is true, smooth the rotated image
                if smooth == True:
                    transImg = transImg.filter(ImageFilter.SMOOTH)

                #Save the old dimensions
                oldWidth = width
                oldHeight = height

                #Paste the image
                canvas.paste(transImg, (int(x + (oldWidth/2 - transImg.width/2)), int(y + (oldHeight/2 - transImg.height/2))), mask = transImg)

            #Create a function to draw cards
            #('number' is the number which should be drawn at the bottom, 'rotation' is the angle the card should be drawn at)
            def drawCard(card, x, y, position = 'bottom', field = False, number = None, rotation = 0):

                #Adjust the x and y coördinates and the rotation when when the position is 'top'
                if position == 'top':
                    x = canvasWidth - x - cardWidth
                    y = canvasHeight - y - cardHeight
                    rotation += 180

                #Retrieve and resize the image
                cardImg = (assets['cardsImg'][card['image']].copy()
                           if (position == 'bottom' or field == True or self.turnPhase in [6, 7])
                              and card.get('flipped', False) != True
                           else find(lambda c: 'Back' in c[0], assets['cardsImg'].items())[1].copy())
                cardImg = cardImg.resize((cardWidth, cardHeight))

                #Draw the numbering on the card
                if number is not None:

                    if number < 10:
                        img = assets['numImg'][number].copy()
                        img = img.resize((int(img.width*cardNumSize), int(img.height*cardNumSize)))

                        #When the position is top, turn the number 180 degrees and apply a relY
                        relX = relX = 2*cardOffset
                        relY = cardHeight - img.height*cardNumSize - cardOffset*3.5
                        if position == 'top':
                            img = img.rotate(180)
                            relY = cardOffset*1.5

                        cardImg.paste(img, (int(relX), int(relY)), mask=img)
                    else:
                        img1 = assets['numImg'][1].copy()
                        img1 = img1.resize((int(img1.width*cardNumSize), int(img1.height*cardNumSize)))
                        img0 = assets['numImg'][0].copy()
                        img0 = img0.resize((int(img0.width*cardNumSize), int(img0.height*cardNumSize)))
                        relX1 = 2*cardOffset
                        relY1 = cardHeight - img1.height*cardNumSize - cardOffset*3.5
                        relX0 = 3.5*cardOffset + img1.width*cardNumSize
                        relY0 = cardHeight - img0.height*cardNumSize - cardOffset*3.5

                        #When the position is top, turn the number 180 degrees and apply another rel X and Y
                        if position == 'top':
                            img1 = img1.rotate(180)
                            relX1 = 4*cardOffset + img0.width*cardNumSize
                            relY1 = cardOffset*1.5
                            img0 = img0.rotate(180)
                            relX0 = 2*cardOffset
                            relY0 = cardOffset*1.5

                        cardImg.paste(img1, (int(relX1), int(relY1)), mask=img1)
                        cardImg.paste(img0, (int(relX0), int(relY0)), mask=img0)

                #Rotate and draw the card
                drawImage(cardImg, x, y, rotation=int(rotation))



            #Draw the fields
            for fieldIndex, field in enumerate(self.stage['fields']):

                #Determine the position where the cards should be drawn
                position = 'bottom' if fieldIndex == pIndex else 'top'

                #Draw the cards
                for i, card in enumerate(field):
                    drawCard(card,
                        i * (cardWidth + fieldCardOverlap) + fieldWidth + (fieldWidthCorrection if position == 'top' else 0),
                        fieldHeight + (fieldHeightCorrection if position == 'top' else 0),
                        position, True
                    )

            #Draw both players's hands
            for handIndex, hand in enumerate(self.stage['hands']):

                #Determine the position where the cards should be drawn
                position = 'bottom' if handIndex == pIndex else 'top'

                #Draw the cards
                for i, card in enumerate(hand):

                    #On the bottom draw card numbers if it's this player's turn and it's the the second phase
                    #or for/both players when it's phase 3/4 and their deck is empty.
                    #Draw the numbers on the top for the current player when it's the fifth phase.
                    number = None
                    if ((self.currentPlayerIndex == pIndex and self.turnPhase == 2 and position == 'bottom')
                        or (self.turnPhase in [3,4] and not len(self.stage['decks'][pIndex]) and position == 'bottom')
                        or (self.currentPlayerIndex == pIndex and self.turnPhase == 5 and position == 'top')):
                        number = i + 1

                    #Setup the width of all the cards and the relative coordinates to draw the card at
                    handCardOverlap = currhandCardOverlap if position == 'bottom' else opphandCardOverlap
                    cardsWidth = self.versionsConfig[self.gameVersion]['cardNumbers']['hand'] * (cardWidth + handCardOverlap) - (cardWidth + handCardOverlap)
                    x = (i + (self.versionsConfig[self.gameVersion]['cardNumbers']['hand'] - len(self.stage['hands'][handIndex]))/2) * (cardWidth + handCardOverlap)

                    #Only when the number of cards are more than 3 calculate the height and angle for the card by using a parabola
                    y = 0
                    dy = 0
                    # y = ax**2 + bx
                    # 0 = a*cardsWidth**2 + b*cardsWidth
                    # cardsHeight = a*1/2*cardsWidth + b*1/2*cardsWidth
                    # b*1/2*cardsWidth = cardsHeight - a*1/2*cardsWidth
                    # 0 = a*cardsWidth**2 + (cardsHeight - a*1/2*cardsWidth)*2
                    # 0 = a*cardsWidth**2 - a*cardsWidth + 2*cardsHeight
                    # -2*cardsHeight = a*cardsWidth**2 - a*cardsWidth
                    # -2*cardsHeight = a(cardsWidth**2 - cardsWidth)
                    a = -2*cardsHeight/(cardsWidth**2 - cardsWidth)
                    b = (cardsHeight - a*(1/2)*cardsWidth)/(1/2*cardsWidth)
                    y = -(a*x**2 + b*x)
                    dy = (cardsHeight/80)*(2*a*x + b)

                    #Draw the card
                    absX = x + (handWidth - (cardsWidth - handCardOverlap)/2) + (handWidthCorrection if position == 'top' else 0)
                    absY = y + handHeight + (handHeightCorrection if position == 'top' else 0)
                    drawCard(card, absX, absY, position, False, number, dy*180)

            #Display the total points for both players
            for playerIndex, points in enumerate([self.getTotalPoints(0), self.getTotalPoints(1)]):

                #Get the images for the numbers
                pointImg = [self.assets['numImg'][int(num)] for num in list(str(points))]
                previousNumWidth = 0

                #Loop over every number to draw it
                for i in range(len(pointImg)):

                    #Get the correct height
                    y = canvasHeight/2 + (pointsRelHeight if playerIndex == pIndex else - (pointsRelHeight + pointImg[i].height + pointsHeightCorrection))

                    #Draw the number
                    pointsWidth = 0
                    for point in pointImg:
                        pointsWidth += point.width
                    drawImage(pointImg[i],
                        canvasWidth/2 - pointsWidth/2 + previousNumWidth + pointsWidthCorrection,
                        y)

                    #Add to the present width
                    previousNumWidth += pointImg[i].width

            #Draw the player border, pf and name for his opponent
            drawImage(assets['playerBorder'], canvas.width - assets['playerBorder'].width*playerBorderSize, 0, assets['playerBorder'].width*playerBorderSize, assets['playerBorder'].height*playerBorderSize)
            pf = assets['playersImg'][oppIndex]
            drawImage(pf,
                canvas.width - assets['playerBorder'].width*playerBorderSize*(9/24) - pfWidth*playerBorderSize/2,
                (97/100)*assets['playerBorder'].height*playerBorderSize - pfHeight*playerBorderSize,
                pfWidth*playerBorderSize, pfHeight*playerBorderSize)
            drawImage(assets['nameBorder'],
                canvas.width - assets['nameBorder'].width*playerBorderSize,
                (91/100)*assets['playerBorder'].height*playerBorderSize - assets['nameBorder'].height*playerBorderSize)
            name = self.players[oppIndex]['player'].name
            nameWidth, nameHeight = ctx.textsize(name, font=nameFont)
            ctx.text((canvas.width - assets['nameBorder'].width*playerBorderSize/2 - nameWidth/2, (163/200)*assets['playerBorder'].height*playerBorderSize),
                 self.players[oppIndex]['player'].name,
                 font = nameFont,
                 fill = colorWhite
                )

            #Draw the decks
            backCard = find(lambda c: c['name'] == 'Back', self.cards)
            if len(self.stage['decks'][pIndex]) > 0:
                drawCard(backCard, currDeckWidth, handHeight, 'bottom')
            if len(self.stage['decks'][oppIndex]) > 0:
                drawCard(backCard, oppDeckWidth, handHeight + handHeightCorrection/2, 'top')

            #Draw the speech bubble when there is text available in the opposing player's property
            if self.players[oppIndex]['speech'] is not None:
                bubbleText = self.players[oppIndex]['speech']
                bubbleHeight = (53/100)*assets['playerBorder'].height
                bubbleWidth = canvas.width - (50/100)*assets['playerBorder'].width

                #Draw the right of the speech bubble
                drawImage(assets['bubbleRight'], bubbleWidth, bubbleHeight)

                #Draw the middle of the speech bubble (which varies by length, depending on the amount of characters)
                textWidth = ctx.textsize(bubbleText, font=speechFont)[0]**1
                bubbleMiddleWidth = 1 if textWidth - assets['bubbleRight'].width/2 - assets['bubbleLeft'].width/3 <= 0 else textWidth - assets['bubbleRight'].width/2 - assets['bubbleLeft'].width/3
                bubbleWidth -= bubbleMiddleWidth
                drawImage(assets['bubbleMiddle'], bubbleWidth, bubbleHeight, bubbleMiddleWidth, assets['bubbleMiddle'].height)

                #Draw the left of the speech bubble
                bubbleWidth -= assets['bubbleLeft'].width
                drawImage(assets['bubbleLeft'], bubbleWidth, bubbleHeight)

                #Draw the text of the speech bubble
                speechWidth, speechHeight = ctx.textsize(bubbleText, font=speechFont)
                ctx.text((bubbleWidth + borderOffset + assets['bubbleRight'].width/8,
                         bubbleHeight + (62/100)*assets['bubbleRight'].height - nameHeight),
                         bubbleText,
                         font=speechFont,
                         fill=colorWhite
                         )

                #Set the speech bubble property to None
                self.players[oppIndex]['speech'] = None

            #When it's phase 6, display a victory message for the winner and a lost message for the loser
            #and when it's phase 7, a draw message for both users.
            if self.turnPhase in [6,7]:
                msg = ((self.assets['winMsg'] if pIndex == self.currentPlayerIndex else self.assets['loseMsg'])
                       if self.turnPhase == 6 else self.assets['drawMsg'])
                drawImage(msg, canvasWidth/2 - msg.width/2, canvasHeight/2 - msg.height/2)

            #Convert the canvas to a file object
            canvasFile = BytesIO()
            canvas.save(canvasFile, "JPEG")
            canvasFile.seek(0)

            #Send the message
            msg = await p['player'].send(description, file = discord.File(canvasFile, 'blade.jpg'))

            #Delete the previous message and save the new msg into the match object
            asyncio.ensure_future(p['msg'].delete())
            p['msg'] = msg

        #Carry out the send function for both players
        asyncio.ensure_future(send(0))
        asyncio.ensure_future(send(1))


    #Other function that can be used to only change the description of an already send embed of a specific player
    #Should be used when the description changes during a phase
    async def changeDescription(self, pIndex, newDesc):

        # #First retrieve the msg that was sent
        # msg = self.players[pIndex]['msg']
        #
        # #Then recreate the embed that was send with a changed description
        # embed = msg.embeds[0]
        # embed.description = newDesc

        #Then change the original message
        # await self.players[pIndex]['msg'].edit(embed = embed)
        await self.players[pIndex]['msg'].edit(content=newDesc)

