# Import modules
from redbot.core import commands, Config
from .matchModule import Match
import json
import discord
import os
import re

# Retrieve the path to the cog, retrieve the config and save the cog path into the config
cogPath = re.sub(r'[\w.]+$', '', os.path.realpath(__file__)).replace('\\', '/')
with open(f'{cogPath}gameConfig.json') as file:
    bladeConfig = json.loads(file.read())
    bladeConfig['cogPath'] = cogPath

#Declare command class
class Blade(commands.Cog):
    """Command to play the Blade Card Game."""

    @commands.command()
    async def blade(self, ctx, mention, opt_game_version = '2'):
        """Invite someone to play Blade.

        Place a mention `@<username>` after the command to invite someone. The game will take place in the private channels with the bot. During the game you can choose which card to play by sending a number ranging from 1-10.

        Optionally, you may specify which Blade version you want to play by placing '1' or '2' after the mention (defaults to Blade 2). Compared to Blade 2, Blade 1 has some fewer (special) cards.

        A common problem is that invited users don't have have private messages enabled from server members.

        To get an explanation on the game do the command `bladeinfo` or go visit the wiki: https://kiseki.fandom.com/wiki/Blade."""

        #Retrieve the invitating user and invitated user
        player1 = ctx.author
        player2 = ctx.message.mentions[0] if len(ctx.message.mentions) else None

        #When no user can be found send a default message
        if player2 is None:
            return await player1.send('I couldn\'t find the mentioned user! Try to be in the same server as the user.')

        #When someone tries to invite himself, let the user know
        if player1.id == player2.id and player1.name != 'Ramanel':
            return await player1.send('You can\'t invite yourself! Are you feeling lonely or something?')

        #Send the mentioned user an invitation to a match, but only if they aren't a bot
        inviteMsg = None
        if not player2.bot:
            try:
                inviteMsg = await player2.send(player1.name + ' has sent you an invitation to play Blade ' + ('1' if opt_game_version == '1' else '2') + '. Do you accept?')
            except discord.DiscordException:
                await player1.send('The user you tried to invite, probably has disabled direct messages from server members. If you want to invite the person, he/she must enable this option.')
                return
        else:
            await player1.send('The user you tried to invite can\'t receive any messages.')
            return

        #Add reactions to message
        await inviteMsg.add_reaction('✅')
        await inviteMsg.add_reaction('❌')

        # Wait for an reaction to the invite message
        def verifyReaction(r, u):
            return not u.bot and r.message.id == inviteMsg.id and r.emoji in ['✅', '❌']

        reaction, user = await ctx.bot.wait_for('reaction_add', check = verifyReaction, timeout = bladeConfig['inviteExpireTime'])

        #When the user reacts with a cross send the original user an message
        if reaction.emoji == '❌':
            await player1.send(player2.name + ' has refused your invitation.')

        #If the user reacts with a ok sign, delete the invite and create and start a new match
        elif reaction.emoji == '✅':
            match = Match(ctx.bot, player1, player2, opt_game_version, bladeConfig)
            await match.start()

        # Delete the invite message
        await inviteMsg.delete()

    @commands.command()
    async def bladeinfo(self, ctx):
        """Explanation of the rules of the Blade Card Game."""

        await ctx.channel.send('In this game the deck consists of bright **cards** ranging from **1 to 7 points** and a handful of darker **special cards**.' +
        ' Players alternate turns by playing cards by sending numbers (1-10) to **raise their point value above their opponent\'s**.' +
        ' **The game ends** whenever one of the players **can\'t score higher** than their opponent' +
        ' (either because the player\'s cards are not of a high enough value or one of the players has run out of cards).' +
        ' **The victor** is the one who has scored the **most points** in the end.' +
        '\n\nThere are also some special cards and rules:' +
        '\n-At the start of the game and whenever the player\'s **points are equal**, the **cards** on the table are **removed** and both players **draw a new card** from their deck (which has 5-6 cards). The player with the lowest card begins.' +
        '\n-**Special cards** (which don\'t have any number) only take **effect when played **from the hand**. When they are drawn they are all worth 1 point.' +
        '\n-When you are left with **only special cards** you automatically **lose**.' +
        '\n-The special **\'Mirror\'** card **switches** all played **cards** between both players.' +
        '\n-The special **\'Bolt\'** card can be used to **undo** (flip) the opponent\'s **last played card**.' +
        '\n-The normal **\'Rod\'** card (with the number one) **reactivates the top most card** card disabled by Bolt. When no flipped card is present the card counts as 1 points.' +
        '\n-(Only in Blade 2) The special **\'Force\'** card (with the purple imbued sword) **doubles your points**.' +
        '\n-(Only in Blade 2) The special **\'Blast\'** card (with the whirlwind sword) enables you to remove one random card from your opponent\'s hand and then take another turn.' +
        '\n-If you have made it to the end, I want to tell you that if you enclose a message within \'quotes\' (\' or \") that message will appear to the opponent in a funny way...'
        '\n\nYou can also find some more info on: https://kiseki.fandom.com/wiki/Blade.')