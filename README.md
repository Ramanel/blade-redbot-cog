# Discord Blade Card Game Cog

This is a cog developed for [RedBot](https://github.com/Cog-Creators/Red-DiscordBot) to be able to play the fast paced Blade card game as found in the Legend of Heroes: Trails of Cold Steel games.
Info on the rules can be found [here](https://kiseki.fandom.com/wiki/Blade).

# Contents

- blade command: Do `[p]blade @<username> <opt_game_version>` to invite a user for a match. Optionally, you can specify the game version ('1' or '2'). Blade 1 (found in Trails of Cold Steel 1) has a bunch of fever (special) cards than Blade 2 (found in Cold Steel 2).
- blade info command: Do `[p]bladeinfo` to get an overview of the in-game rules.

I don't recommend changing the configuration found in `gameConfig.json`, but if you do, do it at your own risk of breaking the cog.


# Installation

1. Install the repo (https://gitlab.com/Ramanel/blade-redbot-cog).
2. Place the cog folder somewhere in you bot (preferably where you store other cogs).
3. Add the path to the folder where the cog folder is located: `[p]addpath <path>`
4. Load the cog: `[p]load <cog_name>`
5. Display the help about the cog or it's commands: `[p]help <cog/command_name>`

# Contact

You can find me on Discord as Ramanel#0650.